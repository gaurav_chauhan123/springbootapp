package io.java.springboot.topics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class TopicService {

	private List<Topic> topics = new ArrayList<>(Arrays.asList(

			new Topic("Spring","1","Spring APP"),
			new Topic("Java","2","JAVA APP"),
			new Topic("Groovy","3","Groovy APP"),
			new Topic("Gradle","4","Gradle APP")

			));

	public List<Topic> getAllTopics(){
		return topics;
	}

	/*public Topic getTopicbyID(String id){
		return topics.stream().filter(t -> t.getId().equals(id)).findFirst().get();
	}*/

	public void addTopic(Topic topic) {
		topics.add(topic);
	}

	public void updateTopicsList(String id, Topic topic) {

		System.out.println("Inside update topic list" + topic.getId());
		for(int i = 0; i < topics.size(); i++){

			Topic t  = topics.get(i);
			if(t.getId().equals(id)){
				System.out.println("Details Matched!!");
				topics.set(i, topic);
				return;
			}
		}
	}

	/*public void deleteTopicsbyID(String id) {
		topics.removeIf(t -> t.getId().equals(id));
	}*/
}
