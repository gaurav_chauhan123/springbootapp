package io.java.springboot.hello;

import java.util.HashSet;
import java.util.Set;

public class Test {

	/*static String find(String input){
		
		//String input = "AAABBBBCC";

		

		if(input.length()>0){
			int count = 1;

			char last = input.charAt(0);

			StringBuilder output = new StringBuilder();
			for(int i = 1; i < input.length(); i++){
			    if(input.charAt(i) == last){
			    count++;
			    }else{
			        if(count >= 1){
			            output.append(""+count+last);
			        }else{
			            output.append(last);
			        }
			    count = 1;
			    last = input.charAt(i);
			    }
			}
			if(count >= 1){
			    output.append(""+count+last);
			}else{
			    output.append(last);
			}
			System.out.println(output.toString());
			
			return output.toString();
			
		}else{
			return "";
		}
		
		
		
	}*/
	
	public static Set<String> permutation(String str) {
        Set<String> perm = new HashSet<String>();
        if (str == null) {
            return null;
        } else if (str.length() == 0) {
            perm.add("");
            return perm;
        }
        char initial = str.charAt(0);
        String rem = str.substring(1);
        Set<String> words = permutation(rem);
        for (String strNew : words) {
            for (int i = 0;i<=strNew.length();i++){
                perm.add(charInsert(strNew, initial, i));
            }
        }
        System.out.println("The size is "+ perm.size());
        return perm;
	}
	
	public static String charInsert(String str, char c, int j) {
        String begin = str.substring(0, j);
        String end = str.substring(j);
        return begin + c + end;
    }
	
static long calculatePossibleCombinations(String input) {
      
	 Long l;
     Set<String> s =    permutation(input);

     l = (long) s.size();
     System.out.println(l);
     return l;

    }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//find("qqqqiiiiddd");
		
		 String s = "2101";
	        String s1 = "ABC";
	        String s2 = "ABCD";
	        System.out.println("\nPermutations for " + s + " are: \n" + calculatePossibleCombinations(s));

	}

}
