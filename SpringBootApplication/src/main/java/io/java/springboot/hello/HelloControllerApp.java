package io.java.springboot.hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloControllerApp {

	@RequestMapping("/hello")
	public String sayHi(){
		
		return "Hi";
	}
	
	
	@RequestMapping("/hey")
	public String sayHey(){
		
		return "Hey";
	}
}
