package io.java.springboot.register;

public class Template {

		private String username;
		private String password;
		private String name;
		private String address;
		private String email;
		private int mobileNo;
		private int pincode;
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public int getMobileNo() {
			return mobileNo;
		}
		public void setMobileNo(int mobileNo) {
			this.mobileNo = mobileNo;
		}
		public int getPincode() {
			return pincode;
		}
		public void setPincode(int pincode) {
			this.pincode = pincode;
		}
		
}
