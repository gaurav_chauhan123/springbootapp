package io.java.springboot.register;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import io.java.springboot.login.Login;

@Entity
@Table(name ="registerUser")
public class Register {
	
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long regid;
	private String name;
	private String address;
	private String email;
	private int mobileNo;
	private int pincode;
	
	@OneToOne(mappedBy = "register",optional = false,cascade = CascadeType.ALL)
	private Login login;
	
	public Register() {
		
	}
	
	public Register(String name, String address, String email, int mobileNo,
			int pincode, Long regid, Login login) {
		super();
		this.name = name;
		this.address = address;
		this.email = email;
		this.mobileNo = mobileNo;
		this.pincode = pincode;
		this.regid = regid;
		this.login = login;
	}
	
	
	
	public Long getRegid() {
		return regid;
	}

	public void setRegid(Long regid) {
		this.regid = regid;
	}

	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(int mobileNo) {
		this.mobileNo = mobileNo;
	}
	public int getPincode() {
		return pincode;
	}
	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}
}
