package io.java.springboot.login;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface LoginRepository extends JpaRepository<Login, Long>{

	
}
