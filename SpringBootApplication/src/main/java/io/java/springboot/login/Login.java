package io.java.springboot.login;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import io.java.springboot.register.Register;

@Entity
@Table(name = "LoginTable")
public class Login {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long id;
	
	@Column(name = "username")
	private String username;
	private String password;
	
	 @OneToOne(fetch = FetchType.LAZY)
	 private Register register;
	
	public Login() {
		
	}
		
	public Login(String username, String password, Long id) {
		super();
		this.username = username;
		this.password = password;
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	//@OneToOne(mappedBy = "login")
	public Register getRegister() {
		return register;
	}

	public void setRegister(Register register) {
		this.register = register;
	}
	
	
	
}
