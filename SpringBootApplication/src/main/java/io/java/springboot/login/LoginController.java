package io.java.springboot.login;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import io.java.springboot.register.Register;
import io.java.springboot.register.RegisterRepository;
import io.java.springboot.register.Template;

@RestController
public class LoginController {

	@Autowired
	private LoginRepository loginRepository;
	
	@Autowired
	private RegisterRepository registerRepository;
	
	 @Autowired
	 private BCryptPasswordEncoder bCryptPasswordEncoder;
	 

	@RequestMapping(method=RequestMethod.POST,value="/log")
	public String login(@RequestBody Login login){
		String encoded = bCryptPasswordEncoder.encode(login.getPassword()); //DatatypeConverter.printBase64Binary(login.getPassword().getBytes());
		boolean flag = true;
		List<Login> log = (List<Login>) loginRepository.findAll();
		System.out.println("the log is "+log);
		for (Login login2 : log) {
			if(login2.getUsername().equalsIgnoreCase(login.getUsername())){
				flag = false;
			}
		}
		if(flag){
			login.setPassword(encoded);
			loginRepository.save(login);
			return "details added succesfully";
		}else{
			return "Username already exist";
		}
	}

	@RequestMapping(method=RequestMethod.POST, value="/login")
	public String loginValidation(@RequestBody Login login){

		System.out.println("The password is "+ login.getPassword());
		String flag = "Login_Failed";
		List<Login> log = (List<Login>) loginRepository.findAll();
		System.out.println("the log is "+log);
		for (Login login2 : log) {
			if(bCryptPasswordEncoder.matches(login.getPassword(), login2.getPassword()) 
					&& login2.getUsername().equalsIgnoreCase(login.getUsername())){
				flag =  "Login_Success";
			}
		}
		return flag;
	}
	
	@RequestMapping("/test")
	public String test(){
		return "echo test";
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/register")
	public String register(@RequestBody Template template){
		Login login =  new Login();
		Register register = new Register();
		String encoded = bCryptPasswordEncoder.encode(template.getPassword()); //DatatypeConverter.printBase64Binary(login.getPassword().getBytes());
		System.out.println("Encoded value is \t" + encoded);
		boolean flag = true;
		List<Login> log = (List<Login>) loginRepository.findAll();
		for (Login login2 : log) {
			if(login2.getUsername().equalsIgnoreCase(template.getUsername())){
				flag = false;
			}
		}
		if(flag){
			login.setUsername(template.getUsername());
			login.setPassword(encoded);
			register.setEmail(template.getEmail());
			register.setAddress(template.getAddress());
			register.setName(template.getName());
			register.setMobileNo(template.getMobileNo());
			register.setPincode(template.getPincode());
			register.setLogin(login);
			registerRepository.save(register);
			return "details added succesfully";
		}else{
			return "Username already exist";
		}
	}
	
}
